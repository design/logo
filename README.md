# Logos for general usage:

![](logo.png)

[SVG](logo.svg) Streamlined vector graphics ready for the web. Adjust `width=""` and `height=""` if needed.

[PNG](logo.png) 120x120px sized pixel graphics with white background.

---

![](logo-brand.png)

[PNG](logo-brand.png)

[SVG](logo-brand.svg)
